// @flow
/* eslint-disable */

import React from 'react';
import { createRoot } from 'react-dom/client';
import { Component, sharedComponentData } from 'react-simplified';

class ItemStore {
  items: string[] = [];

  pending = true;

  load(): Promise<void> {
    return new Promise((resolve) => {
      Info.set('loading items');
      this.pending = true;
      // Simulate time-consuming task
      setTimeout(() => {
        this.items = ['Orange', 'Apple', 'Lemon'];
        Info.set('loaded items');
        this.pending = false;
        resolve();
      }, 500);
    });
  }

  add(item: string): Promise<void> {
    return new Promise((resolve) => {
      Info.set('adding item');
      this.pending = true;
      // Simulate time-consuming task
      setTimeout(() => {
        this.items.push(item);
        Info.set('added item');
        this.pending = false;
        resolve();
      }, 500);
    });
  }

  clear(): Promise<void> {
    return new Promise((resolve) => {
      Info.set('clearing items');
      this.pending = true;
      // Simulate time-consuming task
      setTimeout(() => {
        this.items = [];
        Info.set('cleared items');
        this.pending = false;
        resolve();
      }, 500);
    });
  }
}
const itemStore = sharedComponentData(new ItemStore());

class ItemForm extends Component {
  newItem = '';

  setNewItem(event: SyntheticInputEvent<HTMLInputElement>) {
    this.newItem = event.currentTarget.value;
  }

  addNewItem() {
    itemStore.add(this.newItem).then(() => (this.newItem = ''));
  }

  render(): React$Node {
    return (
      <fieldset disabled={itemStore.pending}>
        <input type="text" value={this.newItem} onChange={this.setNewItem} />
        <button onClick={this.addNewItem}>Add item</button>
        <button onClick={itemStore.clear}>Clear items</button>
      </fieldset>
    );
  }
}

class ItemList extends Component {
  render(): React$Node {
    return (
      <div style={{ opacity: itemStore.pending ? '0.5' : '1.0' }}>
        <ul>
          {itemStore.items.map((item, i) => (
            <li key={i}>{item}</li>
          ))}
        </ul>
      </div>
    );
  }
}

class Items extends Component {
  render(): React$Node {
    return (
      <div>
        <ItemForm />
        <ItemList />
      </div>
    );
  }

  mounted() {
    itemStore.load();
  }
}

class Info extends Component {
  message = '';

  render(): React$Node {
    return <div>Info: {this.message}</div>;
  }

  static set(message: string) {
    for (const instance of Info.instances()) {
      instance.message = message;
    }
  }
}

const root = document.getElementById('root');
if (root) {
  createRoot(root).render(
    <div>
      <Info />
      <Items />
    </div>,
  );
}
