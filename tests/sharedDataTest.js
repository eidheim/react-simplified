import * as React from 'react';
import TestRenderer from 'react-test-renderer';
import { Component, sharedComponentData } from '../src/index';

class Store {
  items = ['Orange', 'Apple', 'Lemon'];

  addItem(item) {
    this.items.push(item);
  }

  clearItems() {
    this.items = [];
  }
}
const store = sharedComponentData(new Store());

class App extends Component {
  newItem = '';

  render() {
    return (
      <div>
        <div>
          <input
            id="input"
            type="text"
            value={this.newItem}
            onChange={(event) => (this.newItem = event.currentTarget.value)}
          />
          <button
            id="addButton"
            onClick={() => {
              store.addItem(this.newItem);
              this.newItem = '';
            }}
          >
            Add item
          </button>
          <button id="clearButton" onClick={store.clearItems}>
            Clear items
          </button>
        </div>
        <div>
          <ul id="list">
            {store.items.map((item, i) => (
              <li key={i}>{item}</li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

describe('sharedComponentData tests', () => {
  const testRenderer = TestRenderer.create(<App />);

  it('Initially', () => {
    expect(testRenderer.getInstance().newItem).toEqual('');
    expect(testRenderer.root.findByProps({ id: 'input' }).props.value).toEqual('');
    expect(testRenderer.root.findByProps({ id: 'list' }).children.length).toEqual(3);
    expect(testRenderer.root.findByProps({ id: 'list' }).children[0].children).toEqual(['Orange']);
    expect(testRenderer.root.findByProps({ id: 'list' }).children[1].children).toEqual(['Apple']);
    expect(testRenderer.root.findByProps({ id: 'list' }).children[2].children).toEqual(['Lemon']);
  });

  it('After input', () => {
    testRenderer.root
      .findByProps({ id: 'input' })
      .props.onChange({ currentTarget: { value: 'test' } });

    expect(testRenderer.getInstance().newItem).toEqual('test');
    expect(testRenderer.root.findByProps({ id: 'input' }).props.value).toEqual('test');
  });

  it('After adding item', () => {
    testRenderer.root.findByProps({ id: 'addButton' }).props.onClick();

    expect(testRenderer.getInstance().newItem).toEqual('');
    expect(testRenderer.root.findByProps({ id: 'input' }).props.value).toEqual('');
    expect(testRenderer.root.findByProps({ id: 'list' }).children.length).toEqual(4);
    expect(testRenderer.root.findByProps({ id: 'list' }).children[0].children).toEqual(['Orange']);
    expect(testRenderer.root.findByProps({ id: 'list' }).children[1].children).toEqual(['Apple']);
    expect(testRenderer.root.findByProps({ id: 'list' }).children[2].children).toEqual(['Lemon']);
    expect(testRenderer.root.findByProps({ id: 'list' }).children[3].children).toEqual(['test']);
  });

  it('After clearing items', () => {
    testRenderer.root.findByProps({ id: 'clearButton' }).props.onClick();

    expect(testRenderer.root.findByProps({ id: 'list' }).children.length).toEqual(0);
  });
});
