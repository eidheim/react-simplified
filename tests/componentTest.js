import * as React from 'react';
import TestRenderer from 'react-test-renderer';
import { Component, sharedComponentData } from '../src/index';

const time = sharedComponentData({ seconds: 0 });
const interval = setInterval(() => time.seconds++, 1000);
afterAll(() => {
  clearInterval(interval);
});

let mountedCount = 0;
let beforeUnmountCount = 0;
class App extends Component {
  times = [];
  showAddButton = false;
  div = null;
  ref = React.createRef();
  counterInObject = { counter: 1 };
  notUsed = 'not used';
  timeout1 = null;
  timeout2 = null;

  render() {
    return (
      <div ref={(e) => (this.div = e)}>
        <div id="time">{time.seconds}</div>
        <div id="button">
          {this.showAddButton ? <button onClick={this.onAddButtonClick}>Add</button> : null}
        </div>
        <div id="times">
          <ul>
            {this.times.map((time, i) => (
              <li key={i}>{time}</li>
            ))}
          </ul>
        </div>
        <div id="counterInObject">{this.counterInObject.counter}</div>
      </div>
    );
  }

  onAddButtonClick() {
    this.times.push(time.seconds);
  }

  mounted() {
    if (this.props.match) {
      this.props.match.params.id;
    }

    mountedCount++;

    this.timeout1 = setTimeout(() => {
      this.showAddButton = true;
      this.counterInObject = { counter: 2 };
    }, 500);

    this.timeout2 = setTimeout(() => {
      this.counterInObject.counter = 3;
    }, 2000);
  }

  beforeUnmount() {
    beforeUnmountCount++;
    clearTimeout(this.timeout1);
    clearTimeout(this.timeout2);
  }
}
let initialInstance = App.instance();
let initialInstances = App.instances();

class Unmounted extends Component {
  render() {
    return null;
  }
}

class ErroneousComponent extends Component {
  shouldThrow;

  render() {
    return <div>{this.shouldThrow}</div>;
  }
}

class ComponentWithUpdated extends Component {
  mountedCount = 0;
  updatedCount = 0;
  beforeUnmountCount = 0;

  render() {
    return null;
  }

  mounted() {
    if (this.props.match) {
      this.props.match.params.id;
    }

    this.mountedCount++;
  }

  beforeUnmount() {
    this.beforeUnmountCount++;
  }

  updated() {
    this.updatedCount++;
  }
}

let componentWillUnmountCount = 0;
class RegularLifecycleHooks extends Component {
  componentDidMountCount = 0;

  render() {
    return null;
  }

  componentDidMount() {
    if (this.props.match) {
      this.props.match.params.id;
    }

    this.componentDidMountCount++;
  }

  componentWillUnmount() {
    componentWillUnmountCount++;
  }
}
class RegularLifecycleHooksWithComponentDidUpdate extends Component {
  componentDidMountCount = 0;
  componentDidUpdateCount = 0;

  render() {
    return null;
  }

  componentDidMount() {
    if (this.props.match) {
      this.props.match.params.id;
    }

    this.componentDidMountCount++;
  }

  componentDidUpdate() {
    this.componentDidUpdateCount++;
  }

  componentWillUnmount() {
    componentWillUnmountCount++;
  }
}

class EmptyComponent extends Component {}

describe('App tests', () => {
  const testRenderer = TestRenderer.create(<App />);

  it('Member variable tests', () => {
    const _reactSimplifiedVariables = testRenderer.getInstance()._reactSimplifiedVariables;
    expect(_reactSimplifiedVariables.has('times')).toEqual(true);
    expect(_reactSimplifiedVariables.has('showAddButton')).toEqual(true);
    expect(_reactSimplifiedVariables.has('div')).toEqual(true);
    expect(_reactSimplifiedVariables.has('ref')).toEqual(false);
    expect(_reactSimplifiedVariables.has('counterInObject')).toEqual(true);
    expect(_reactSimplifiedVariables.has('notUsed')).toEqual(true);
    const _reactSimplifiedRerenderVariables =
      testRenderer.getInstance()._reactSimplifiedRerenderVariables;
    expect(_reactSimplifiedRerenderVariables.has('times')).toEqual(true);
    expect(_reactSimplifiedRerenderVariables.has('showAddButton')).toEqual(true);
    expect(_reactSimplifiedRerenderVariables.has('div')).toEqual(false);
    expect(_reactSimplifiedRerenderVariables.has('ref')).toEqual(false);
    expect(_reactSimplifiedRerenderVariables.has('counterInObject')).toEqual(true);
    expect(_reactSimplifiedRerenderVariables.has('notUsed')).toEqual(false);
  });

  it('Initial render', () => {
    expect(testRenderer.root.findByProps({ id: 'time' }).children).toEqual(['0']);
    expect(testRenderer.root.findByProps({ id: 'counterInObject' }).children).toEqual(['1']);
    expect(testRenderer.root.findByProps({ id: 'button' }).children.length).toEqual(0);
    expect(testRenderer.root.findByProps({ id: 'times' }).children[0].children.length).toEqual(0);
  });

  it('After 1.5 seconds', (done) => {
    setTimeout(() => {
      expect(time.seconds).toEqual(1);
      expect(testRenderer.root.findByProps({ id: 'time' }).children).toEqual(['1']);
      expect(testRenderer.root.findByProps({ id: 'counterInObject' }).children).toEqual(['2']);
      expect(testRenderer.root.findByProps({ id: 'button' }).children.length).toEqual(1);
      expect(testRenderer.root.findByProps({ id: 'times' }).children[0].children.length).toEqual(0);

      testRenderer.root.findByProps({ id: 'button' }).children[0].props.onClick();
      expect(
        testRenderer.root.findByProps({ id: 'times' }).children[0].children[0].children
      ).toEqual(['1']);
      done();
    }, 1500);
  });

  it('After another 1 second', (done) => {
    setTimeout(() => {
      expect(time.seconds).toEqual(2);
      expect(testRenderer.root.findByProps({ id: 'time' }).children).toEqual(['2']);
      expect(testRenderer.root.findByProps({ id: 'counterInObject' }).children).toEqual(['3']);
      done();
    }, 1000);
  });

  it('props.match.params.id added', () => {
    expect(beforeUnmountCount).toEqual(0);
    expect(mountedCount).toEqual(1);
    testRenderer.update(<App match={{ params: { id: 1 } }} />);
    expect(beforeUnmountCount).toEqual(1);
    expect(mountedCount).toEqual(2);
  });

  it('props.match unchanged', () => {
    testRenderer.update(<App match={{ params: { id: 1 } }} />);
    expect(beforeUnmountCount).toEqual(1);
    expect(mountedCount).toEqual(2);
  });

  it('props.match.params.id changed', () => {
    testRenderer.update(<App match={{ params: { id: 2 } }} />);
    expect(beforeUnmountCount).toEqual(2);
    expect(mountedCount).toEqual(3);
  });

  it('props.match unchanged, but other props changed', () => {
    testRenderer.update(<App match={{ params: { id: 2 } }} match2={{ params: { id: 1 } }} />);
    expect(beforeUnmountCount).toEqual(2);
    expect(mountedCount).toEqual(3);

    testRenderer.update(<App match={{ params: { id: 2 }, params2: { id: 1 } }} />);
    expect(beforeUnmountCount).toEqual(2);
    expect(mountedCount).toEqual(3);

    testRenderer.update(<App match={{ params: { id: 2, id2: 1 } }} />);
    expect(beforeUnmountCount).toEqual(2);
    expect(mountedCount).toEqual(3);

    testRenderer.update(<App match={{ params: { id: 2 } }} />);
    expect(beforeUnmountCount).toEqual(2);
    expect(mountedCount).toEqual(3);
  });

  it('props.match.params.id set to object', () => {
    testRenderer.update(<App match={{ params: { id: {} } }} />);
    expect(beforeUnmountCount).toEqual(3);
    expect(mountedCount).toEqual(4);
  });

  it('props.match set to null', () => {
    testRenderer.update(<App match={null} />);
    expect(beforeUnmountCount).toEqual(4);
    expect(mountedCount).toEqual(5);
  });

  it('props.match.params.id added again', () => {
    testRenderer.update(<App match={{ params: { id: 1 } }} />);
    expect(beforeUnmountCount).toEqual(5);
    expect(mountedCount).toEqual(6);
  });

  it('Before mounting a component, instance() should return null and instances() should return []', () => {
    expect(initialInstance).toEqual(null);
    expect(initialInstances).toEqual([]);
    expect(Unmounted.instance()).toEqual(null);
    expect(Unmounted.instances()).toEqual([]);
  });

  it('instance() and instances() should return valid instances', () => {
    expect(App.instance()).toEqual(testRenderer.getInstance());
    expect(App.instances()).toEqual([testRenderer.getInstance()]);
  });

  it('instance() and instances() should return correct instances after mounting a second App component, and beforeUnmount should be called before unmount', () => {
    const testRenderer2 = TestRenderer.create(<App />);

    expect(App.instance()).toEqual(testRenderer2.getInstance());
    expect(App.instances()).toEqual([testRenderer.getInstance(), testRenderer2.getInstance()]);

    testRenderer2.unmount();

    expect(beforeUnmountCount).toEqual(6);
  });

  it('Finally, after unmount, instance() and instances() should return null and [] respectively, and beforeUnmount should be called', () => {
    testRenderer.unmount();

    expect(App.instance()).toEqual(null);
    expect(App.instances()).toEqual([]);

    expect(beforeUnmountCount).toEqual(7);
  });
});

describe('ErroneousComponent tests', () => {
  let didThrow = false;
  try {
    TestRenderer.create(<ErroneousComponent />);
  } catch (e) {
    didThrow = true;
  }
  it('Components using undefined properties should throw', () => {
    expect(didThrow).toEqual(true);
  });
});

describe('ComponentWithUpdated tests, automatic props handling should be disabled when updated() is defined', () => {
  it('mounted and beforeUnmount should only be called once', () => {
    const testRenderer = TestRenderer.create(<ComponentWithUpdated />);
    expect(testRenderer.getInstance().mountedCount).toEqual(1);
    expect(testRenderer.getInstance().beforeUnmountCount).toEqual(0);
    expect(testRenderer.getInstance().updatedCount).toEqual(0);

    testRenderer.update(<ComponentWithUpdated match={{ params: { id: 1 } }} />);
    expect(testRenderer.getInstance().mountedCount).toEqual(1);
    expect(testRenderer.getInstance().beforeUnmountCount).toEqual(0);
    expect(testRenderer.getInstance().updatedCount).toEqual(1);

    testRenderer.update(<ComponentWithUpdated match={{ params: { id: 2 } }} />);
    expect(testRenderer.getInstance().mountedCount).toEqual(1);
    expect(testRenderer.getInstance().beforeUnmountCount).toEqual(0);
    expect(testRenderer.getInstance().updatedCount).toEqual(2);
  });
});

describe('RegularLifecycleHooks and RegularLifecycleHooksWithComponentDidUpdate tests', () => {
  it('componentDidMount and componentWillUnmount should be called', () => {
    componentWillUnmountCount = 0;
    const testRenderer = TestRenderer.create(<RegularLifecycleHooks />);
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(1);
    expect(componentWillUnmountCount).toEqual(0);

    testRenderer.update(<RegularLifecycleHooks match2={{ params: { id: 1 } }} />);
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(1);
    expect(componentWillUnmountCount).toEqual(0);

    testRenderer.update(<RegularLifecycleHooks match={{ params: { id: 1 } }} />);
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(2);
    expect(componentWillUnmountCount).toEqual(1);

    testRenderer.update(<RegularLifecycleHooks match={{ params: { id: 2 } }} />);
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(3);
    expect(componentWillUnmountCount).toEqual(2);

    let instance = testRenderer.getInstance();
    testRenderer.unmount();
    expect(instance.componentDidMountCount).toEqual(3);
    expect(componentWillUnmountCount).toEqual(3);
  });

  it('componentDidMount, componentDidUpdate and componentWillUnmount should be called', () => {
    componentWillUnmountCount = 0;
    const testRenderer = TestRenderer.create(<RegularLifecycleHooksWithComponentDidUpdate />);
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(1);
    expect(testRenderer.getInstance().componentDidUpdateCount).toEqual(0);
    expect(componentWillUnmountCount).toEqual(0);

    testRenderer.update(
      <RegularLifecycleHooksWithComponentDidUpdate match2={{ params: { id: 1 } }} />
    );
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(1);
    expect(testRenderer.getInstance().componentDidUpdateCount).toEqual(1);
    expect(componentWillUnmountCount).toEqual(0);

    testRenderer.update(
      <RegularLifecycleHooksWithComponentDidUpdate match={{ params: { id: 1 } }} />
    );
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(1);
    expect(testRenderer.getInstance().componentDidUpdateCount).toEqual(2);
    expect(componentWillUnmountCount).toEqual(0);

    testRenderer.update(
      <RegularLifecycleHooksWithComponentDidUpdate match={{ params: { id: 2 } }} />
    );
    expect(testRenderer.getInstance().componentDidMountCount).toEqual(1);
    expect(testRenderer.getInstance().componentDidUpdateCount).toEqual(3);
    expect(componentWillUnmountCount).toEqual(0);

    let instance = testRenderer.getInstance();
    testRenderer.unmount();
    expect(instance.componentDidMountCount).toEqual(1);
    expect(instance.componentDidUpdateCount).toEqual(3);
    expect(componentWillUnmountCount).toEqual(1);
  });

  it('EmptyComponent should not throw', () => {
    TestRenderer.create(<EmptyComponent />);
  });
});
