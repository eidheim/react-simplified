import { Component as ReactComponent } from 'react';
import { observable, observe, unobserve } from '@nx-js/observer-util';

// Detect test environment.
const testEnv =
  typeof process == 'object' &&
  process != null /* eslint-disable-line no-undef */ &&
  typeof process.env == 'object' /* eslint-disable-line no-undef */ &&
  process.env != null /* eslint-disable-line no-undef */ &&
  process.env.NODE_ENV == 'test'; /* eslint-disable-line no-undef */

// Do not bind these component methods.
const bindComponentExcludes = [
  'constructor',
  'render',
  // 'componentDidMount', must be bound in case this function is called in a component method.
  'shouldComponentUpdate',
  'getSnapshotBeforeUpdate',
  'componentDidUpdate',
  // 'componentWillUnmount', must be bound in case this function is called in a component method.
  'componentDidCatch',
  // 'mounted', must be bound in case this function is called in a component method.
  'updated',
  // 'beforeUnmount' must be bound in case this function is called in a component method.
];

// Do not bind these object methods in sharedComponentData.
const bindInstanceExcludes = [
  'constructor',
  '__defineGetter__',
  '__defineSetter__',
  'hasOwnProperty',
  '__lookupGetter__',
  '__lookupSetter__',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toString',
  'valueOf',
  '__proto__',
  'toLocaleString',
];

// Do not make these properties observable.
const observableExcludes = ['props', 'context', 'refs', 'updater', 'state'];

const sharedComponentData = (object) => {
  let shared = observable(object);

  // Bind methods.
  for (const name of Object.getOwnPropertyNames(Object.getPrototypeOf(object))) {
    if (!bindInstanceExcludes.includes(name)) {
      shared[name] = shared[name].bind(shared);
    }
  }

  return shared;
};

// The same observable must be used elsewhere or else components will not rerender.
export { sharedComponentData, observe, unobserve };

export class Component extends ReactComponent {
  // Returns the last instance.
  static instance() {
    if (!Component._instances) {
      return null;
    }
    const instances = Component._instances.get(this.name);
    if (!instances || instances.length == 0) {
      return null;
    }
    return instances[instances.length - 1];
  }

  // Returns all current instances.
  static instances() {
    if (!Component._instances) {
      return [];
    }
    const instances = Component._instances.get(this.name);
    if (!instances) {
      return [];
    }
    return instances;
  }

  constructor(...args) {
    super(...args);

    // Store new instance.
    if (!Component._instances) {
      Component._instances = new Map();
    }
    let instances = Component._instances.get(this.constructor.name);
    if (!instances) {
      Component._instances.set(this.constructor.name, [this]);
    } else {
      instances.push(this);
    }

    // Bind methods.
    for (const name of Object.getOwnPropertyNames(Object.getPrototypeOf(this))) {
      if (!name.startsWith('UNSAFE_component') && !bindComponentExcludes.includes(name)) {
        this[name] = this[name].bind(this);
      }
    }

    // Store this.render as it will be overwritten to detect used properties during render call and create property setters.
    const render = this.render;

    let variables = new Map();
    let rerenderVariables = new Set();

    // Store the following objects in instance for testing purposes.
    if (testEnv) {
      this._reactSimplifiedVariables = variables;
      this._reactSimplifiedRerenderVariables = rerenderVariables;
    }

    /*
     * During render, make property objects observable, and create getters and setters for properties.
     * Observable objects enable detecting when substructures change and rerender should be scheduled.
     * Updated non-objects will directly cause rerender scheduling if used in render.
     */
    let firstRenderCall = true;
    let duringRenderCall = false;
    this.render = () => {
      if (firstRenderCall) {
        firstRenderCall = false;

        const ownPropertyNames = Object.getOwnPropertyNames(this);
        for (const name of ownPropertyNames) {
          const type = typeof this[name];
          if (
            type != 'function' &&
            !name.startsWith('_react') &&
            !observableExcludes.includes(name)
          ) {
            // Detect undefined properties
            if (type == 'undefined') {
              throw new Error(
                'Detected undefined member variable ' +
                  name +
                  ' in class ' +
                  this.constructor.name +
                  '. Since such variables are unsafe to use in render(), you must initialize this variable.',
              );
            }

            // Make property observable if it is object.
            if (type == 'object' && this[name] != null) {
              // Do not make React.createRef() objects observable.
              if (Object.isSealed(this[name])) {
                let keys = Object.keys(this[name]);
                if (keys.length == 1 && keys[0] == 'current') {
                  continue;
                }
              }

              variables.set(name, observable(this[name]));
            } else {
              variables.set(name, this[name]);
            }

            // If property is used during render call, schedule rerender when property is changed.
            Object.defineProperty(this, name, {
              // When property is read.
              get: () => {
                if (duringRenderCall) {
                  rerenderVariables.add(name);
                }

                return variables.get(name);
              },
              // When property is written to.
              set: (value) => {
                const rerender = rerenderVariables.has(name);

                variables.set(
                  name,
                  typeof value == 'object' && value != null && rerender ? observable(value) : value,
                );

                // Schedule rerender
                if (rerender) {
                  this.setState({});
                }
              },
            });
          }
        }
      }

      /*
       * Call original render function.
       * The boolean duringRenderCall is used to identify properties that should be cause rerender scheduling.
       */
      duringRenderCall = true;
      let result;
      if (render) {
        result = render.call(this);
      }
      duringRenderCall = false;

      return result;
    };

    // Schedule rerender when observable objects change.
    this.render = observe(this.render, {
      scheduler: () => this.setState({}),
      lazy: true,
    });

    // Storage for this.componentDidMount.
    let componentDidMount;
    // Stores used prop keys in a tree for fast traversal. A node is a map of child key to child node.
    let rootNode;

    /*
     * Detect this.props used in componentDidMount.
     * When these props change, they will cause new calls to componentWillUnmount and componentDidMount.
     */
    if (this.mounted || this.componentDidMount) {
      componentDidMount = this.componentDidMount;
      this.componentDidMount = () => {
        // References to parent nodes containing used prop keys.
        let parentPropToParentNode = new Map();
        let propToProxy = new Map();
        let handlers = {
          get(target, key, receiver) {
            // Get true value from target, that is always using getter. See https://stackoverflow.com/a/39003741.
            let result = Reflect.get(target, key, receiver);

            // Create new node if it does not exist
            let parentNode = parentPropToParentNode.get(target);
            if (!parentNode.has(key)) {
              let node = new Map();
              parentNode.set(key, node);
              parentPropToParentNode.set(result, node);
            }

            if (typeof result == 'object' && result != null) {
              // If proxy is already created, return it instead of making a new proxy.
              let proxy = propToProxy.get(result);
              if (proxy) {
                return proxy;
              }

              proxy = new Proxy(result, handlers);
              propToProxy.set(result, proxy);
              return proxy;
            }
            return result;
          },
        };
        let storedProps = this.props;

        // Only shallow copy needed since freeze and seal are shallow.
        let mutableProps = Object.assign({}, this.props);
        rootNode = new Map();
        parentPropToParentNode.set(mutableProps, rootNode);
        this.props = new Proxy(mutableProps, handlers);

        let result;
        if (this.mounted) {
          result = this.mounted();
        } else if (componentDidMount) {
          result = componentDidMount();
        }

        this.props = storedProps;
        return result;
      };
    }

    // Disable rerender when component is about to be unmounted.
    const componentWillUnmount = this.componentWillUnmount;
    this.componentWillUnmount = () => {
      let instances = Component._instances.get(this.constructor.name);
      instances.splice(instances.indexOf(this), 1);

      unobserve(this.render);

      if (this.beforeUnmount) {
        return this.beforeUnmount();
      } else if (componentWillUnmount) {
        return componentWillUnmount();
      }
    };

    // Calls componentWillUnmount and componentDidMount when props that are used in componentDidMount changes.
    const componentDidUpdate = this.componentDidUpdate;
    this.componentDidUpdate = (prevProps, prevState, snapshot) => {
      if (
        !this.updated && // This method might already handle calling componentWillUnmount or componentDidMount.
        !componentDidUpdate && // This method might already handle calling componentWillUnmount or componentDidMount.
        this.componentDidMount &&
        this.props != prevProps
      ) {
        const equal = (prevProps, props, parentNode) => {
          for (const [key, node] of parentNode) {
            const prevValue = prevProps[key];
            const value = props[key];

            // If objects, check if one object is null, or else compare non-object values.
            if (
              typeof prevValue == 'object' && typeof value == 'object'
                ? (prevValue != null && value == null) || (prevValue == null && value != null)
                : prevValue !== value
            ) {
              return false;
            }

            if (!equal(prevProps[key], props[key], node)) {
              return false;
            }
          }
          return true;
        };

        // Call componentWillUnmount and componentDidMount if props used during componentDidMount have changed.
        if (!equal(prevProps, this.props, rootNode)) {
          if (this.beforeUnmount) {
            this.beforeUnmount();
          } else if (componentWillUnmount) {
            componentWillUnmount();
          }

          this.componentDidMount();
        }
      }

      if (this.updated) {
        return this.updated(prevProps, prevState, snapshot);
      } else if (componentDidUpdate) {
        return componentDidUpdate.call(this, prevProps, prevState, snapshot);
      }
    };
  }
}
