# react-simplified

Implements automatic state and props management for React components using
[ES6 Proxies](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy)
and the [@nx-js/observer-util](https://github.com/nx-js/observer-util) library.

## Features

- Rerenders a component if a member variable changes that affects renderering
- Can create shared data between components, through the `sharedComponentData()` function, that
  rerenders affected components when altered
- Calls `componentWillUnmount()` and `componentDidMount()` when props used in `componentDidMount()`
  changes
- Auto-binds component methods
- Static methods `instance()` and `instances()` that simplifies static method implementations
- Can use lifecycle hooks `mounted()`, `updated()` and `beforeUnmount()` instead of
  `componentDidMount()`, `componentDidUpdate()` and `componentWillUnmount()`
- Detects use of undefined member variables that can cause issues in `render()`
- Works with React Native
- Both Flow and TypeScript supported

## Installation

```sh
npm install react-simplified
```

## Examples

Example showing updated date and time:

```jsx
import React from 'react';
import { Component } from 'react-simplified';
import { createRoot } from 'react-dom/client';

class App extends Component {
  date = new Date();

  render() {
    return <div>{this.date.toString()}</div>;
  }

  mounted() {
    setInterval(() => (this.date = new Date()), 1000);
  }
}

createRoot(document.getElementById('root')).render(<App />);
```

Example with data shared between two components:

```jsx
import React from 'react';
import { Component, sharedComponentData } from 'react-simplified';
import { createRoot } from 'react-dom/client';

const shared = sharedComponentData({ date: new Date() });
setInterval(() => (shared.date = new Date()), 1000);

class Component1 extends Component {
  render() {
    return <div>{shared.date.toString()}</div>;
  }
}

class Component2 extends Component {
  render() {
    return <div>{shared.date.toString()}</div>;
  }
}

createRoot(document.getElementById('root')).render(
  <>
    <Component1 />
    <Component2 />
  </>,
);
```

A larger example where a class instance, simulating loading and storing data, is shared and used by
two components:

```jsx
import React from 'react';
import { Component, sharedComponentData } from 'react-simplified';
import { createRoot } from 'react-dom/client';

class ItemStore {
  items = [];
  pending = true;

  load() {
    return new Promise((resolve) => {
      this.pending = true;
      // Simulate time-consuming task
      setTimeout(() => {
        this.items = ['Orange', 'Apple', 'Lemon'];
        this.pending = false;
        resolve();
      }, 500);
    });
  }

  add(item) {
    return new Promise((resolve) => {
      this.pending = true;
      // Simulate time-consuming task
      setTimeout(() => {
        this.items.push(item);
        this.pending = false;
        resolve();
      }, 500);
    });
  }

  clear() {
    return new Promise((resolve) => {
      this.pending = true;
      // Simulate time-consuming task
      setTimeout(() => {
        this.items = [];
        this.pending = false;
        resolve();
      }, 500);
    });
  }
}
const itemStore = sharedComponentData(new ItemStore());

class ItemForm extends Component {
  newItem = '';

  setNewItem(event) {
    this.newItem = event.currentTarget.value;
  }

  addNewItem() {
    itemStore.add(this.newItem).then(() => (this.newItem = ''));
  }

  render() {
    return (
      <fieldset disabled={itemStore.pending}>
        <input type="text" value={this.newItem} onChange={this.setNewItem} />
        <button onClick={this.addNewItem}>Add item</button>
        <button onClick={itemStore.clear}>Clear items</button>
      </fieldset>
    );
  }
}

class ItemList extends Component {
  render() {
    return (
      <div style={{ opacity: itemStore.pending ? '0.5' : '1.0' }}>
        <ul>
          {itemStore.items.map((item, i) => (
            <li key={i}>{item}</li>
          ))}
        </ul>
      </div>
    );
  }

  mounted() {
    itemStore.load();
  }
}

createRoot(document.getElementById('root')).render(
  <>
    <ItemForm />
    <ItemList />
  </>,
);
```

See
[examples/src/index.js](https://gitlab.com/eidheim/react-simplified/blob/master/examples/src/index.js)
for additional code samples. These examples can be run from a terminal:

```sh
git clone https://gitlab.com/eidheim/react-simplified
cd react-simplified/examples
npm install
npm start
```

## Documentation

Documentation can be found in the library definitions for
[Flow](https://gitlab.com/eidheim/react-simplified/blob/master/src/index.js.flow) and
[TypeScript](https://gitlab.com/eidheim/react-simplified/blob/master/src/index.d.ts).

## Contributing

Contributions are welcome, either by creating an issue or a merge request. However, before you
create a new issue or merge request, please search for previous similar issues or requests. A
response will normally be given within a few days.
